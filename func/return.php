<?PHP
function hideErrors(){return(error_reporting(E_ALL ^ E_NOTICE));}
function special($str,$limitlength,$minimalexplode,$maximalexplode){//,80,4,6 Use for special substring
	$ret="";
	$len=strlen($str);
	$exp=explode(" ",$str);
	if($len>$limitlength){
		if(count($exp)<$minimalexplode){
			$ret=substr($str,0,$limitlength);
		}
		else{
			for($i=0;$i<$maximalexplode;$i++){
				$ret.=$exp[$i]." ";
			}
		}
	}
	else{
		$ret=$str;
	}
	return($ret);
}
function nbsp($jumlah){$str="";for($i=1;$i<=$jumlah;$i++){$str.="&nbsp;";}return($str);}
function getFileType($file){//get type of file (fileName,
	$etype=explode("/",$_FILES['foto']['type']);
	$type=".".$etype[(count($etype)-1)];
	return($type);
}
function muf($temp,$file){
	return(move_uploaded_file($temp,$file));
}
function fe($file){
	return(file_exists($file));
}
function checkset($var){//not yet 
	if(!isset($var)){
		$var=null;
	}
	return($var);
}
function replace($obj,$text){//change if object is null
	if($obj==NULL){
		$obj=$text;
	}
	return($obj);
}
function convertWhen($date,$granularity=2) {
	$t_date=$date;
    $date = strtotime($date);
	$dates=strtotime(date("d-M-Y h:m:s"));
    $beda = $date -$dates ;
	//still second
	if($beda<0){
		list($tgl_kom,$bln_kom,$thn_kom,$jam_kom)=convertDate($t_date);//function month convertion\
		$ket="$dates $date beda";
	}
	else if ($beda==0) {
        $ket = "1 detik yang lalu";
    }
    else if ($beda<30) {
        $ket = "$beda detik yang lalu";
    }
	else if($beda<60){
		$ket="Beberapa detik yang lalu";
	}
	else if($beda<3600){
		$beda /=60;
		$beda=(int)$beda;
		$ket="$beda menit yang lalu";
	}
	else if($beda<86400){
		$beda /=3600;
		$beda=(int)$beda;
		$ket="$beda menit yang lalu ?";
	}
	else{
		list($tgl_kom,$bln_kom,$thn_kom,$jam_kom)=convertDate($t_date);//function month convertion\
		$ket="$tgl_kom $bln_kom $thn_kom";
	}
	return $ket;   
}
function convertDate($e){
	$a2=substr($e,8,2);
	$b2=substr($e,5,2);
	$c=substr($e,0,4);
	$d=substr($e,11,5);
	$a=(int)$a2;
	switch($b2){
		case"01":$b="Januari";break;
		case"02":$b="Februari";break;
		case"03":$b="Maret";break;
		case"04":$b="April";break;
		case"05":$b="Mei";break;
		case"06":$b="Juni";break;
		case"07":$b="Juli";break;
		case"08":$b="Agustus";break;
		case"09":$b="September";break;
		case"10":$b="Oktober";break;
		case"11":$b="November";break;
		case"12":$b="Desember";break;
		default:$b="Januari";break;
	}
	return array($a,$b,$c,$d);
}
function breakDate($date){
	$len=strlen($date);
	$a=substr($date,8,2);
	$b=substr($date,5,2);
	$c=substr($date,0,4);
	if($len==10){
		return array($a,$b,$c);
	}
	else{
		$d=substr($date,11,2);
		$e=substr($date,14,2);
		$f=substr($date,17,2);
		return array($a,$b,$c,$d,$e,$f);
	}
}
function checkScript($get,$url){
	$get=addslashes($get);
	$cget=strlen($get);
	$c=-1;
	$d=1;
	for($i=0;$i<$cget;$i++){
		$c++;
		$e=substr($get,$c,$d);
		if($e=="\'"||$e=="\""||$e=="-"||$e==" "){
			header('location:'.$url); 
		}
	}
}
function getFileName($table,$pk,$firstname){//check foto (update photo) (get name of photo)
	$com="select $pk from $table order by $pk desc limit 0,1";
	$res=msq($com);
	$cres=mnr($res);
	$foto=$firstname;
	if($cres==0){
		$foto.="1";
	}
	else{
		$id=getIndex($table,$pk);
		$foto.=$id;
	}
	return($foto);
}
function getIndex($table,$pk){
	$com="select $pk from $table";
	$res=msq($com);
	$num=mnr($res);
	$id="";
	if($num>0){
		$i=1;
		while($arr=mfa($res)){
			if($arr[$pk]!=$i){
				break;
			}
			$i++;
		}
		$id=$i;
	}
	else{
		$id="1";
	}
	return($id);
}

function dot($u){
	$ua=$u;
    $tu=0;
	$index=strlen($ua);
    $uang="";
    while($index>0){
		if($tu==3){
			$uang=".".$uang;
			$tu=0;
		}
		$tu++;
		$uang=substr($ua,$index-1,1)."$uang";
    	$index--;
    }
	return($uang);
}
function arrangeDate($a){//arange 01-01-2011
	$d=substr($a,8,2);
	$m=substr($a,5,2);
	$y=substr($a,0,4);
	return array($d,$m,$y);
}
function stringReplace($string,$from,$to){	
	$string = preg_replace("/".preg_quote($from, "/")."/i", "$to", $string);
    return $string;
}
function e($a){
	return(escstr($a));
}
function p($a){//$_POST[];
	return($_POST[$a]);
}
function ep($a){
	return(e($_POST[$a]));
}
function g($a){//get
	return($_GET[$a]);
}//create self function
?>