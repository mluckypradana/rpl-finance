SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
CREATE DATABASE keuangan_6_21 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE keuangan_6_21;

CREATE TABLE IF NOT EXISTS akun (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO akun (id, username, `password`) VALUES
(1, 'admin', 'admin');

CREATE TABLE IF NOT EXISTS barang_pendapatan (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_laporan int(11) NOT NULL,
  id_barang int(11) NOT NULL,
  harga int(16) NOT NULL,
  jumlah int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS barang_pengeluaran (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_laporan int(11) DEFAULT NULL,
  id_barang int(11) DEFAULT NULL,
  nama varchar(32) DEFAULT NULL,
  harga int(16) DEFAULT NULL,
  jumlah int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS jenis_panduan (
  id int(1) NOT NULL AUTO_INCREMENT,
  nama varchar(10) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

INSERT INTO jenis_panduan (id, nama) VALUES
(1, 'Web'),
(2, 'Android');

CREATE TABLE IF NOT EXISTS jk (
  id int(1) NOT NULL AUTO_INCREMENT,
  jk varchar(10) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

INSERT INTO jk (id, jk) VALUES
(1, 'Laki-laki'),
(2, 'Perempuan');

CREATE TABLE IF NOT EXISTS kategori_kegiatan (
  id int(3) NOT NULL AUTO_INCREMENT,
  nama varchar(32) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO kategori_kegiatan (id, nama) VALUES
(1, 'Open House');

CREATE TABLE IF NOT EXISTS kategori_laporan (
  id int(11) NOT NULL AUTO_INCREMENT,
  jenis int(11) NOT NULL,
  nama varchar(32) NOT NULL,
  data_barang int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS kegiatan (
  id int(4) NOT NULL AUTO_INCREMENT,
  nama varchar(50) DEFAULT NULL,
  kategori int(2) DEFAULT NULL,
  deskripsi tinytext,
  tanggal date NOT NULL,
  foto varchar(11) NOT NULL,
  modal int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO kegiatan (id, nama, kategori, deskripsi, tanggal, foto, modal) VALUES
(1, 'Open House 2013', 1, 'Open House 2013 yang dilaksanakan pada tanggal 22 April tahun 2013 hingga pada tanggal 25 April tahun 2013. Kegiatan ini dimaksudkan untuk memperingati hari ulang tahun SMK Negeri 1 Cimahi', '2013-02-05', 'k0.jpg', 5000000);

CREATE TABLE IF NOT EXISTS kota (
  id int(2) NOT NULL AUTO_INCREMENT,
  nama varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

INSERT INTO kota (id, nama) VALUES
(1, 'Cimahi'),
(2, 'Bandung');

CREATE TABLE IF NOT EXISTS laporan (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_kegiatan int(4) DEFAULT NULL,
  kategori int(11) NOT NULL,
  nama varchar(50) DEFAULT NULL,
  tanggal date NOT NULL,
  uang int(12) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS panduan (
  id int(11) NOT NULL AUTO_INCREMENT,
  jenis int(1) DEFAULT NULL,
  isi text,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS rincian_barang (
  id int(11) NOT NULL,
  id_barang int(11) NOT NULL,
  deskripsi text NOT NULL,
  tanggal date NOT NULL,
  foto varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS rincian_penjualan (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_barang int(11) NOT NULL,
  nama varchar(32) NOT NULL,
  alamat text NOT NULL,
  hp varchar(14) NOT NULL,
  tanggal date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `user` (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_akun int(11) NOT NULL,
  nama_depan varchar(32) DEFAULT NULL,
  nama_belakang varchar(32) DEFAULT NULL,
  jk int(1) DEFAULT NULL,
  tempat_lahir varchar(20) DEFAULT NULL,
  tgl_lahir date DEFAULT NULL,
  alamat varchar(200) DEFAULT NULL,
  kota int(2) DEFAULT NULL,
  telp varchar(12) DEFAULT NULL,
  foto varchar(100) DEFAULT NULL,
  tgl_daftar timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
CREATE TABLE IF NOT EXISTS `view_kegiatan` (
`id` int(4)
,`nama` varchar(50)
,`id_kategori` int(3)
,`kategori` varchar(32)
,`deskripsi` tinytext
,`tanggal` date
,`foto` varchar(11)
,`modal` int(11)
,`pendapatan` bigint(21)
,`pengeluaran` bigint(21)
,`jumlah_pendapatan` decimal(48,0)
,`jumlah_pengeluaran` decimal(48,0)
);CREATE TABLE IF NOT EXISTS `view_laporan` (
`id` int(11)
,`id_kegiatan` int(4)
,`nama_kegiatan` varchar(50)
,`jenis` int(11)
,`barang` int(11)
,`kategori` varchar(32)
,`nama` varchar(50)
,`tanggal` date
,`uang` int(12)
,`pendapatan` decimal(47,0)
,`pengeluaran` decimal(47,0)
);DROP TABLE IF EXISTS `view_kegiatan`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW view_kegiatan AS select kegiatan.id AS id,kegiatan.nama AS nama,kategori_kegiatan.id AS id_kategori,kategori_kegiatan.nama AS kategori,kegiatan.deskripsi AS deskripsi,kegiatan.tanggal AS tanggal,kegiatan.foto AS foto,kegiatan.modal AS modal,(select count(0) from laporan where ((laporan.id_kegiatan = kegiatan.id) and laporan.kategori in (select kategori_laporan.id from kategori_laporan where (kategori_laporan.jenis = 1)))) AS pendapatan,(select count(0) from laporan where ((laporan.id_kegiatan = kegiatan.id) and laporan.kategori in (select kategori_laporan.id from kategori_laporan where (kategori_laporan.jenis = 2)))) AS pengeluaran,(select (sum((barang_pendapatan.harga * barang_pendapatan.jumlah)) + (select sum(laporan.uang) from laporan where (laporan.kategori in (select kategori_laporan.id from kategori_laporan where (kategori_laporan.jenis = 1)) and (laporan.id_kegiatan = kegiatan.id)))) from barang_pendapatan where barang_pendapatan.id_laporan in (select laporan.id from laporan where (laporan.kategori in (select kategori_laporan.id from kategori_laporan where (kategori_laporan.jenis = 1)) and (laporan.id_kegiatan = kegiatan.id)))) AS jumlah_pendapatan,(select (sum((barang_pengeluaran.harga * barang_pengeluaran.jumlah)) + (select sum(laporan.uang) from laporan where (laporan.kategori in (select kategori_laporan.id from kategori_laporan where (kategori_laporan.jenis = 2)) and (laporan.id_kegiatan = kegiatan.id)))) from barang_pengeluaran where barang_pengeluaran.id_laporan in (select laporan.id from laporan where (laporan.kategori in (select kategori_laporan.id from kategori_laporan where (kategori_laporan.jenis = 2)) and (laporan.id_kegiatan = kegiatan.id)))) AS jumlah_pengeluaran from (kegiatan join kategori_kegiatan) where (kegiatan.kategori = kategori_kegiatan.id) group by kegiatan.id;
DROP TABLE IF EXISTS `view_laporan`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW view_laporan AS select laporan.id AS id,laporan.id_kegiatan AS id_kegiatan,kegiatan.nama AS nama_kegiatan,kategori_laporan.jenis AS jenis,kategori_laporan.data_barang AS barang,kategori_laporan.nama AS kategori,laporan.nama AS nama,laporan.tanggal AS tanggal,laporan.uang AS uang,(select sum((barang_pendapatan.harga * barang_pendapatan.jumlah)) from barang_pendapatan where (barang_pendapatan.id_laporan = laporan.id)) AS pendapatan,(select sum((barang_pengeluaran.harga * barang_pengeluaran.jumlah)) from barang_pengeluaran where (barang_pengeluaran.id_laporan = laporan.id)) AS pengeluaran from ((laporan join kategori_laporan) join kegiatan) where (laporan.kategori = kategori_laporan.id) group by laporan.id;
